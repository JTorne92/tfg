# Creació de web per la gestió d'etiquetes QR utilitzant el framework AngularJS

Aquest projecte és una aplicació web que utilitza [AngularJS](http://angularjs.org/).
Aquest projecte és el TFG del Jordi Torné Soler, estudiant de la FIB(UPC). 
Es pot provar el funcionament de l'aplicació en: https://tfgcodisqr.firebaseapp.com .

En aquest projecte s'utilitza el servei de base de dades i hosting Firebase, per tant si vols implementar
la teva aplicació web utilitzant com a base aquest codi, tindràs que crear-te la teva pròpia compte a Firebase(https://www.firebase.com/). 
Llavors faltarà canviar en tots els codis Javascript l'adreça "https://tfgcodisqr.firebaseio.com/" per la teva.

### Prerequisits

Necessites Git i instalar [Nodejs](http://nodejs.org/).

### Clonar el repositori

Per clonar el repositori utilitza [git][git]:

```
git clone git clone https://JTorne92@bitbucket.org/JTorne92/tfg.git
cd CodiTFG
```

### Instal·lar dependències

Per instal·lar les dependències utilitza la següent comanda:
```
npm install
```
Aquesta comanda instal·larà en el directori Bower,Http-Server,Karma i Protractor.
També s'haurà d’instal·lar Firebase:
```
npm install firebase --save
```

### Executar l'aplicació

Només cal fer:

```
npm start
```
Llavors obre amb el navegador `http://localhost:8000/app/index.html`.

[git]: http://git-scm.com/