'use strict';
 
angular.module('myApp.veureConversa', ['ngRoute','firebase'])

.controller('veureConversaCtrl', ['$scope','$routeParams','$firebaseArray','$firebaseObject','$location','$timeout', function($scope,$routeParams,$firebaseArray,$firebaseObject,$location,$timeout){
	$scope.loading=true;
  $scope.userEliminat=false;
  $scope.usersElimnats=" ";
  var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
  	var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ authData.uid);
  	$scope.userSessio= $firebaseObject(refUserSessio);
  	$scope.IDConv =  $routeParams.IDConv;

  	var refLlistUsersConv= new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+$scope.IDConv+"/usuarisConv");
  	$scope.usersConversa= $firebaseArray(refLlistUsersConv);
  	var refMissatgesConv= new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+$scope.IDConv+"/missatges");
  	$scope.missatgesConv= $firebaseArray(refMissatgesConv);
    $scope.conversaUsuari= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+authData.uid+"/converses/"+$scope.IDConv));
    $scope.permis=false;
    $scope.usersConversa.$loaded().then(function() {
      var esta= $scope.usersConversa.$indexFor(authData.uid)
      if(esta!= -1) $scope.permis=true;
      $scope.loading=false;
    })
    //Canviem el número de missatges no vists d'aquesta conversa a zero
    $scope.conversaUsuari.$loaded().then(function() {
        $scope.conversaUsuari.numNoVist=0;
        $scope.conversaUsuari.$save();
        var listener= $scope.conversaUsuari.$watch(function() { 
          //Si rebem un altre missatge i estem en aquesta pantalla llavors ja l'hem vist
          if(location.hash==("#/conversa/"+$scope.IDConv)){
            if($scope.conversaUsuari.numNoVist!=0){
              $scope.conversaUsuari.numNoVist=0;
              $scope.conversaUsuari.$save();
            }
          }
          else{         //Si no estem en la pantalla de la de conversa parem el watch
            listener(); //Per que parar el watch? si no continua fent el watch encara que canvi de pantalla
          }
        });
    })
  	$scope.traduirData= function(d){
    	return new Date(d)
  	}
  	$scope.enviarMissatge= function(){
  		var contingut= $scope.Noumissatge; //El contingut que s'ha introduit pel missatge
      $scope.Noumissatge="";
  		var autorMissatge= $scope.userSessio.username;
  		var today= new Date();
      	var dia= today.getTime()
      	///Afegim el missatge a la llista
  		$scope.missatgesConv.$add({
  			 contingut: contingut,
        	autor: autorMissatge,
        	dataMis: dia
  		})
      $scope.usuarisASumarNovist=[];
      function sumarNoVists(uD){
           var today= new Date();
           var dia= today.getTime()
           uD.numNoVist+=1;
           uD.dataUltim=dia;
           uD.$save();
      }
      function sumarActius(objC){
           objC.contadorUsersActius+=1;
           objC.$save();
      }
      $scope.userRegs= $firebaseArray(new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"));
      $scope.userRegs.$loaded().then(function() {
        for (var i = 0; i < $scope.usersConversa.length; i++) {
            $scope.usersElimnats= "Els següents usuaris han elimanat la seva compte i per tant no rebran els missatges: ";
            if($scope.usersConversa[i].$id != authData.uid){  //L'autor del missatge no s'ha de notificar res ni tornar-li a enviar
              var refUDest= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.usersConversa[i].$id);
              if($scope.userRegs.$indexFor($scope.usersConversa[i].username)== -1){
                  $scope.userEliminat=true;
                  var uE= $scope.usersConversa[i].username;
                  $timeout(function() {
                    $scope.usersElimnats += ( uE+ " ");
                  })
              }
              else if($scope.usersConversa[i].borrat){
                //En els usuaris conversa, mirem si esta borrat --> Llavors tornar-li a ficar la 'FK' i augmentar contador
                var today= new Date();
                var dia= today.getTime()
                refUDest.child('converses').child($scope.IDConv).set({
                    numNoVist: 1,
                    dataUltim: dia
                });
                var ObjectConv= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+$scope.IDConv));
                ObjectConv.$loaded().then(function(objC) {
                  sumarActius(objC)
                })

              }
              else {
               //En tots els usuaris menys l'autor sumar +1 en NMissNoVist
                $scope.usuariDest= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.usersConversa[i].$id + "/converses/"+$scope.IDConv));
                $scope.usuariDest.$loaded().then(function(uD) {
                  sumarNoVists(uD)
                })
              }
            }
            else{
                var usuariAutor= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ authData.uid + "/converses/"+$scope.IDConv));
                usuariAutor.$loaded().then(function(uD) {
                  var today= new Date();
                  var dia= today.getTime()
                  usuariAutor.dataUltim=dia;
                  usuariAutor.$save();
                })
            }
        }
      })
  	}
    $scope.veurePerfil= function(userPerfil){
      var trobat=false;
      var IDusuariPerfil="";
      for (var j = 0; j < $scope.usersConversa.length && !trobat; j++) {
        if($scope.usersConversa[j].username==userPerfil){
          trobat=true;
          IDusuariPerfil=$scope.usersConversa[j].$id;
        }
      }
      $location.path("/perfil/"+IDusuariPerfil); 
    }
}])
