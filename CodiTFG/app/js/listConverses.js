'use strict';
 
angular.module('myApp.listConverses', ['ngRoute','firebase'])

.controller('listConversesCtrl',['$scope','$modal','$location','$firebaseArray','$firebaseObject', function($scope,$modal,$location, $firebaseArray,$firebaseObject){
  $scope.loading=true;
  var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
	$scope.userID=authData.uid;
	var refusersAmics= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/usuarisAmics");
	$scope.usersAmics= $firebaseArray(refusersAmics);
  ///Agafar llista converses teves
  var refConversesTeves= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/converses");
  $scope.conversesTeves= $firebaseArray(refConversesTeves);
  $scope.arrayMissatgesConverses=[]
  $scope.arrayUsuarisConverses=[]
  $scope.infoLastMissatges=[];
  $scope.infoUsuarisConv=[]
  $scope.conversesTeves.$loaded().then(function() {
      if($scope.conversesTeves.length>0)llegirConverses();
      else{
        $scope.loading=false;
      }
      //Actualitzem les nostres converses si hi ha un canvi en alguna de les nostres 
      var listener=$scope.conversesTeves.$watch(function() { 
          if(location.hash== "#/listConv"){ 
            $scope.arrayMissatgesConverses=[]
            $scope.arrayUsuarisConverses=[]
            $scope.infoLastMissatges=[];
            $scope.infoUsuarisConv=[]
            llegirConverses()
          }
          else{          //Si no estem en la pantalla de llista de converses parem el watch
            listener(); //Per que parar el watch? si no continua fent el watch encara que canvi de pantalla
          }
      });
  })
  $scope.traduirData= function(d){
    return new Date(d)
  }
  function infoLastMissatges(arrayM){
    //Quan s'ha cargat les teves converses, agafem la informació del últim missatge de cada conversa
    var k = $scope.arrayMissatgesConverses.indexOf(arrayM)
    var missaThisConv= $scope.arrayMissatgesConverses[k];
    var infoMiss= {
      autor: missaThisConv[missaThisConv.length-1].autor,
      contingut: missaThisConv[missaThisConv.length-1].contingut,
      dataMis: missaThisConv[missaThisConv.length-1].dataMis
    }
    $scope.infoLastMissatges[k]= infoMiss;
    $scope.loading=false;
  }
  function llistaUsuarisConv(arrayU){
    var q = $scope.arrayUsuarisConverses.indexOf(arrayU)
    var aux="";
    for(var w=0; w<$scope.arrayUsuarisConverses[q].length;w++){
      //Creem un String amb tots els noms d'usuari de la conversa
        aux= aux+ $scope.arrayUsuarisConverses[q][w].username;
        if(w!=$scope.arrayUsuarisConverses[q].length-1){
          aux= aux+ ", "
        }
    }
    $scope.infoUsuarisConv.push(aux)
  }
  function llegirConverses(){
    $scope.conversesTeves.sort(function(a,b) {
       return a.dataUltim>b.dataUltim ? -1 : a.dataUltim<b.dataUltim ? 1 : 0;
    });
    for (var i = 0; i < $scope.conversesTeves.length; i++) {
        var IDConv= $scope.conversesTeves[i].$id //ID de la conversa
        var NoVist= $scope.conversesTeves[i].numNoVist; //# de missatges no vists de la conversa
        $scope.usurisConv= $firebaseArray(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ IDConv +"/usuarisConv"));
        $scope.arrayUsuarisConverses.push($scope.usurisConv) //Guardem els usuaris d'aquesta conversa per agafar-los despres
        $scope.missatgesConversa= $firebaseArray(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ IDConv +"/missatges"));
        $scope.arrayMissatgesConverses.push($scope.missatgesConversa) //Guardem els missatges d'aquesta conversa per agafar l'últim missatge despres
        $scope.arrayMissatgesConverses[i].$loaded().then(function(arrayM) {
            infoLastMissatges(arrayM)
        })
        $scope.arrayUsuarisConverses[i].$loaded().then(function(arrayU) {
            llistaUsuarisConv(arrayU)
        })
    }
  }
	$scope.open = function () {
      var modalInstance = $modal.open({
        templateUrl: 'NovaConversaContent.html',
        controller: 'NovaConversaInstanceCtrl',
        resolve: {
	          userID: function(){
	            return $scope.userID;
	          },
	          userAmics: function(){
	          	return $scope.usersAmics;
	          }
	        }
    	});
    	modalInstance.result.then(function (idConversa) {
        $location.path("/conversa/"+idConversa); 
    	}, function () {
      		console.log("Model dismiss");
    	});
    	$scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
  	};


    $scope.openElimina = function (indElim, idConversaElim) {
      var modalInstance = $modal.open({
        templateUrl: 'EliminarConvContent.html',
        controller: 'ElimConversaInstanceCtrl',
      });
      modalInstance.result.then(function () {
          //En /converses/$IDConversa, decrementar contador, si zero eliminar tota la conversa (tenim el id de la conversa)
          $scope.convEliminar= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ idConversaElim));
          $scope.convEliminar.$loaded().then(function() {
            if($scope.convEliminar.contadorUsersActius<2){
              $scope.convEliminar.$remove()
            }
            else{
              $scope.convEliminar.contadorUsersActius= $scope.convEliminar.contadorUsersActius-1;
              $scope.convEliminar.$save()
              $scope.usurisConvElim= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ idConversaElim +"/usuarisConv/"+$scope.userID));
              //Si no /converses/$IDConversa/usaurisConv/$UserID/borrat=true
              //És necessari "borrat" per despres si un usuari que no ha borrat la conversa, pugui tornar-la a enviar a aquest usuari.
              $scope.usurisConvElim.$loaded().then(function() {
                  $scope.usurisConvElim.borrat=true;
                  $scope.usurisConvElim.$save()
              })
            }
          })
          //Eliminar conversa de /users/$UserID/converses(tenim l'index del array converses teves...)
          $scope.conversesTeves.$remove(indElim)
      }, function () {
          console.log("Model dismiss");
      });
      $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
          if (modalInstance) {
            modalInstance.dismiss('cancel');
          }
      });
    };

}])

.controller('NovaConversaInstanceCtrl', ['$scope','$modalInstance', '$firebaseObject', 'userID','userAmics','$timeout', function ($scope, $modalInstance, $firebaseObject, userID,userAmics,$timeout) {
  $scope.destinataris=[]; //Borrar despres
  $scope.userID= userID;
  $scope.usersAmics= userAmics;
  $scope.escollitAmic= function(amic){
  	$scope.usernameAfegir= amic;	
  }
  $scope.afegirUsuari= function(nomUsuari){
  	$scope.afgError = false;
    if(nomUsuari== "" || nomUsuari== null){
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="Introdueix un nom d'usuari per afegir.";
        });
    }
    else{ 
      if($scope.destinataris.indexOf(nomUsuari)== -1){
        //Comprova si està a la llista
        var refusers = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+nomUsuari);
        refusers.on("value", function(snapshot) {
          var usuariExistent= snapshot.val();
          if(usuariExistent != null){
            //Comprovar si existeix
            if(usuariExistent.userIDassociat != $scope.userID){
                //Comprovar si ets tu
                $timeout(function() {
                  $scope.destinataris.push(nomUsuari);
                  $scope.usernameAfegir= "";
                });
            }
            else{
                $timeout(function() {
                  $scope.afgError = true;
                  $scope.afgErrorMessage ="L'usuari introduit no pots ser tu.";
                });
                console.log("L'usuari introduit no pots ser tu")
            }
          }  
          else{
            $timeout(function() {
              $scope.afgError = true;
              $scope.afgErrorMessage ="L'usuari no existeix.";
            });
            console.log("L'usuari no existeix")
          }
        }, function (errorObject) {
          console.log("The read failed: " + errorObject.code);
        });
      }
      else{
        console.log("L'usuari ja està en la llista")
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="L'usuari ja està en la llista.";
        });  
      }
    }
  }
  $scope.ok = function () {
    var refConversa = new Firebase("https://tfgcodisqr.firebaseio.com/converses/");
    var length= $scope.destinataris.length +1; //Més 1 perque tu també reps el missatge
    var novaRefConversa = refConversa.push({
      contadorUsersActius: length,
    });
    ///Quan el contador estigui a zero es podra missatge de la BD
    var ConvID = novaRefConversa.key();
    var contingut= $scope.missatge; //El contingut que s'ha introduit pel missatge
    //Agafem el nostre username
    var nomUserObject= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID));
    nomUserObject.$loaded().then(function() {
      var autorMissatge= nomUserObject.username;
      var today= new Date();
      var dia= today.getTime()
      var refMissatgesConv = new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ConvID+"/missatges/");
      ///Creem el primer missatge
      refMissatgesConv.push({
        contingut: contingut,
        autor: autorMissatge,
        dataMis: dia
      });
      //Usuaris de la conversa (sense l'autor)
      $scope.nomdest=[];
      $scope.objDest=[];
      function enviar(Dest){
          var y = $scope.objDest.indexOf(Dest)
          var IDUserDest= $scope.objDest[y].userIDassociat;
          //Guardem en la conversa els usuaris destí, el seu ID i un boolea per saber si l'han borrat de la seva llista
          var nom= $scope.nomdest[y]
          refConversa.child(ConvID).child('usuarisConv').child(IDUserDest).set({
            username: nom,
            borrat: false
          });
          var refUDest= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ IDUserDest);
          //En l'usuari tenim un ID que fa la funció de "Foreign Key" i un boolea per saber si s'ha llegit l'ultim missatge de la conversa
          var today= new Date();
          var dia= today.getTime()
          refUDest.child('converses').child(ConvID).set({
            numNoVist: 1,
            dataUltim: dia
          });
      }
      for (var j = 0; j < $scope.destinataris.length; j++) {
        var nomUsuariDest= $scope.destinataris[j]; //Agafem els usernames que hem introduit
        $scope.nomdest.push(nomUsuariDest)
        var ObjUserIDsDest= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+ nomUsuariDest));
        $scope.objDest.push(ObjUserIDsDest)
        //Necessitem el ID del usuari
        $scope.objDest[j].$loaded().then(function(Dest) {
            enviar(Dest)
        })
      }
      //L'usuari autor es diferent perquè ja sabem el seu ID
      var IDAutor= $scope.userID;
      refConversa.child(ConvID).child('usuarisConv').child(IDAutor).set({
        username: autorMissatge,
        borrat: false
      });
      // A diferencia dels altres, l'autor ha escrit l'últim missatge i per tant està vist
      var refAutor= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ IDAutor);
        refAutor.child('converses').child(ConvID).set({
        numNoVist: 0,
        dataUltim: dia
      });
    })
    $modalInstance.close(ConvID);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  $scope.eliminarUsuari= function(index){
      $scope.destinataris.splice(index, 1);
  }
}])

.controller('ElimConversaInstanceCtrl', ['$scope','$modalInstance', function ($scope, $modalInstance) {
  $scope.ok = function () {
    $modalInstance.close();
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])