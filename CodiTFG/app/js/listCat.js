'use strict';
 
angular.module('myApp.listCat', ['ngRoute','firebase'])
 
 
.controller('listCatCtrl', ['$scope', '$firebaseArray','$firebaseObject' ,'$modal', '$location', function($scope,$firebaseArray,$firebaseObject, $modal,$location) {
	$scope.loading=true;
  var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  var authData = ref.getAuth();
  if(!authData){  //No esta loggejat
    $location.path("/");
  }
  $scope.userID= authData.uid;
  var refUser= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID);
  $scope.userSessio= $firebaseObject(refUser);
  $scope.userSessio.$loaded().then(function() { //L'usuari ha estat esborrat per l'administrador
    if($scope.userSessio.username== undefined){ 
      $location.path("/acces/esborrat");
    }
  })
	$scope.catalegs= {};
		console.log("User " + authData.uid + " is logged in with " + authData.provider + " es l'usuari " + authData.password.email);
  var urlCatalegs = "https://tfgcodisqr.firebaseio.com/users/"+ authData.uid + "/Catalegs";
  var refCat = new Firebase(urlCatalegs);
	$scope.catalegs = $firebaseArray(refCat);
	$scope.numCat = function(){
		return $scope.catalegs.$getIndex().lenght;
	}
  $scope.obrirCat= function(idCat){
    $location.path("/catalegs/"+ $scope.userID +"/"+ idCat);
  }
  $scope.loading=false;
}])
///////////////////Afegir Catàleg///////////////////
.controller('afegirCatCtrl', ['$scope', '$modal', function ($scope, $modal) {
  $scope.cat = {nom: ""};
  $scope.iconFinal = 'fa fa-random';
  $scope.iconTitol = 'Altres';

  $scope.escollit= function(iconSelected,iconTitol){
    $scope.iconFinal= iconSelected;
    $scope.iconTitol= iconTitol;
  }
  $scope.open = function () {
    $scope.cat.nom="";
    var modalInstance = $modal.open({
      templateUrl: 'afegirCatContent.html',
      controller: 'catInstanceCtrl',
      resolve: {
        cat: function () {
          return $scope.cat;
        },
        catalegs: function(){
          return $scope.catalegs
        }
      }
    });
    modalInstance.result.then(function (iconSelected) {
      $scope.selected= iconSelected;
    }, function () {
      console.log("Model dismiss");
    });
    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      if (modalInstance) {
        modalInstance.dismiss('cancel');
      }
    });
  };

}])

.controller('catInstanceCtrl', ['$scope', '$modalInstance','cat', 'catalegs','$firebaseArray', function ($scope, $modalInstance, cat, catalegs, $firebaseArray) {
  $scope.cat = cat;
  $scope.selected = {
    icon: 'fa fa-random',
  };
  $scope.colorSelected= "Blau"
  $scope.colorDropdown= "primary";
  $scope.colorEscollit= function(colorEsc){
    $scope.colorSelected= colorEsc;
    switch ($scope.colorSelected) {
          case "Blau":
              $scope.colorDropdown= "primary";
              break; 
          case "Groc":
              $scope.colorDropdown= "warning";
              break; 
          case "Verd":
              $scope.colorDropdown= "success";
              break; 
          case "Vermell":
              $scope.colorDropdown= "danger";
              break; 
      }
  }
  $scope.ok = function () {
      $scope.catalegs= catalegs;
      var iconAfegir= ($scope.selected.icon);
      var color= "primary";
      switch ($scope.colorSelected) {
          case "Groc":
              color= "warning";
              break; 
          case "Verd":
              color= "success";
              break; 
          case "Vermell":
              color= "danger";
              break; 
      }
      $scope.catalegs.$add({
          nom: $scope.cat.nom ,
          icon: iconAfegir,
          color: color
      });
      $modalInstance.close($scope.selected.icon);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])
////////////////////////////////////Eliminar
.controller('EliminarCatCtrl', ['$scope','$modal', function ($scope, $modal) {
  $scope.open = function (index) {
    var modalInstance = $modal.open({
      templateUrl: 'EliminarCatContent.html',
      controller: 'ElicatInstanceCtrl',
      resolve: {
        catalegs: function(){
          return $scope.catalegs
        },
        ind: function(){
          return index;
        },
      }
    });
    modalInstance.result.then(function () {
    }, function () {
      console.log("Model dismiss");
    });
      $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {   ///Quan es clica el botó del navegador de tirar enrera o endevant 
      if (modalInstance) {
        modalInstance.dismiss('cancel');    //que es tanqui la finestra
      }
    }); 
  };

}])
.controller('ElicatInstanceCtrl', ['$scope','$modalInstance', 'catalegs', 'ind', '$firebaseArray', function ($scope, $modalInstance, catalegs, ind, $firebaseArray) {
  $scope.ok = function () {
      $scope.catalegs= catalegs;
      $scope.catalegs.$remove(ind);  
      $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])
///////////////////Modificar catàleg//////////////////////////
.controller('modificaCat', ['$scope','$modal', function ($scope, $modal) {
  $scope.cat = {nom: "",i: ""}
  if($scope.cataleg.icon == "fa fa-book"){
          $scope.iconFinal= "fa fa-book";
          $scope.iconTitol= "Llibres";
  }
  else if($scope.cataleg.icon == "fa fa-random"){
          $scope.iconFinal= "fa fa-random";
          $scope.iconTitol= "Altres";
  }
  else if($scope.cataleg.icon == "fa fa-folder-o"){
          $scope.iconFinal= "fa fa-folder-o";
          $scope.iconTitol= "Carpetes";
  }
  else if($scope.cataleg.icon == "fa fa-clipboard"){
          $scope.iconFinal= "fa fa-clipboard";
          $scope.iconTitol= "Documents";
  }
  else if($scope.cataleg.icon == "fa fa-archive"){
          $scope.iconFinal= "fa fa-archive";
          $scope.iconTitol= "Arxius";
  }
  else if($scope.cataleg.icon == "fa fa-qrcode"){
          $scope.iconFinal= "fa fa-qrcode";
          $scope.iconTitol= "Codis QR";
  }
  else if($scope.cataleg.icon == "fa fa-cubes"){
          $scope.iconFinal= "fa fa-cubes";
          $scope.iconTitol= "Caixes";
  }
  else if($scope.cataleg.icon == "fa fa-list"){
          $scope.iconFinal= "fa fa-list";
          $scope.iconTitol= "Llista";
  }
  else if($scope.cataleg.icon == "fa fa-film"){
          $scope.iconFinal= "fa fa-film";
          $scope.iconTitol= "Pel·lícules";
  }
  $scope.cat.i= $scope.iconFinal;
  $scope.escollit= function(iconSelected,iconTitol){
    $scope.iconFinal= iconSelected;
    $scope.iconTitol= iconTitol;
  }
  $scope.open = function (index) {
    $scope.cat.nom= $scope.cataleg.nom;
    var modalInstance = $modal.open({
      templateUrl: 'ModificarCatContent.html',
      controller: 'modcatInstanceCtrl',
      resolve: {
        cataleg: function(){
          return $scope.cataleg;
        },
        cat: function(){
          return $scope.cat;
        },
        ind: function(){
          return index;
        },
        catalegs: function(){
          return $scope.catalegs
        }
      }
    });
    modalInstance.result.then(function (iconSelected) {
      $scope.selected= iconSelected;
    }, function () {
      console.log("Model dismiss");
    });
      $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      if (modalInstance) {
        modalInstance.dismiss('cancel');
      }
    });
  };
}])


.controller('modcatInstanceCtrl', ['$scope','$modalInstance', 'cataleg', 'catalegs', 'cat','ind','$firebaseArray', function ($scope, $modalInstance, cataleg, catalegs, cat,ind, $firebaseArray) {
  $scope.cataleg = cataleg;
  $scope.cat= cat;
  $scope.selected = {
    icon: $scope.cat.i
  };
  switch ($scope.cataleg.color){
        case "primary":
            $scope.colorSelected= "Blau";
            $scope.colorDropdown= "primary";
        break; 
        case "warning":
            $scope.colorSelected=  "Groc";
            $scope.colorDropdown= "warning";
        break; 
        case "success":
              $scope.colorSelected= "Verd";
              $scope.colorDropdown= "success";
        break; 
        case "danger":
            $scope.colorSelected= "Vermell";
            $scope.colorDropdown= "danger";
        break; 
  }
  $scope.colorEscollit= function(colorEsc){
    $scope.colorSelected= colorEsc;
      switch ($scope.colorSelected) {
          case "Blau":
              $scope.colorDropdown= "primary";
              break; 
          case "Groc":
              $scope.colorDropdown= "warning";
              break; 
          case "Verd":
              $scope.colorDropdown= "success";
              break; 
          case "Vermell":
              $scope.colorDropdown= "danger";
              break; 
      }
  }
  $scope.ok = function () {
      var color= "primary";
      switch ($scope.colorSelected) {
          case "Groc":
              color= "warning";
              break; 
          case "Verd":
              color= "success";
              break; 
          case "Vermell":
              color= "danger";
              break; 
      }

      var iconAfegir= ($scope.selected.icon);
      $scope.catalegs= catalegs;
      var item= $scope.catalegs[ind];
      item.nom = $scope.cat.nom;
      item.icon = iconAfegir;
      item.color = color;
      $scope.catalegs.$save(ind).then(function() {
          // data has been saved to Firebase
      });
      $modalInstance.close($scope.selected.icon);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])