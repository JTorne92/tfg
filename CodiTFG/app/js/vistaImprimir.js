'use strict';
 
angular.module('myApp.vistaImprimir', ['ngRoute'])

.controller('vistaImprimirCtrl', ['$scope','$routeParams', '$location', function($scope,$routeParams,$location){
	$scope.catid =  $routeParams.catid;
	$scope.userID = $routeParams.userID;
	$scope.itemid =  $routeParams.itemid;
	$scope.mida=  $routeParams.sizeQR;
	$scope.numQRs =  $routeParams.num;
			//Perque no mostri el peu, ja que no el volem per imprimir
		$( "#footer" ).hide();
		$scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
	 		$( "#footer" ).show();
		});
	///Comprovem que es una mida de les establertes
	if(($scope.mida!=100) && ($scope.mida!=200) && ($scope.mida!=300) && ($scope.mida!=400) && ($scope.mida!=450)) {
		$location.path("/veureQR/"+ $scope.userID +"/" +$scope.catid + "/"+ $scope.itemid); 
		//Si no es una mida de les establertes, tornem a la pàgina de veure QR (la URL ha sigut modificada per l'usuari)
	} 
	else{
		var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	  	var authData = ref.getAuth();
	  	if(!authData){  //No esta loggejat
	    	$location.path("/");
	  	}
		$scope.propietari=false;
	  	if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
	    	$scope.propietari=true;
	  	}
	  	else{
	   	 	console.log("No ets el propietari")
	    	$location.path("/");
	  	}
		var url= ("https://tfgcodisqr.firebaseapp.com/#/item/"+ $scope.userID + "/" + $scope.catid + "/" + $scope.itemid);
		if($scope.mida==100){		//Els codis QR de 100x100
				if($scope.numQRs>35) {	//35 es el maxim per aquesta mida
					$scope.numQRs=35;
				}
				for(var i=1; i<= $scope.numQRs; i++){
				$('#qrcode').append( "&nbsp;" );
				$('#qrcode').append( "&nbsp;" );
				var qrcode = new QRCode(document.getElementById("qrcode"), {
					width : $scope.mida,
					height : $scope.mida,
					text: url,
					correctLevel : QRCode.CorrectLevel.H
				});
				$('#qrcode').append( "&nbsp;" );
				$('#qrcode').append( "&nbsp;" );
			 	if(i%5==0 && i!=35){		//només si es de 100x100
			 		$('#qrcode').append( "<br>" );
			 		$('#qrcode').append( "<br>" );
			 		$('#qrcode').append( "<br>" );
			 	}
			}
		}
		else if($scope.mida==200){	//Els codis QR de 200x200
				if($scope.numQRs>12) {	//12 es el maxim per aquesta mida
					$scope.numQRs=12;
				}
				for(var i=1; i<= $scope.numQRs; i++){
					$('#qrcode').append( "&nbsp;" );
					$('#qrcode').append( "&nbsp;" );
				var qrcode = new QRCode(document.getElementById("qrcode"), {
					width : $scope.mida,
					height : $scope.mida,
					text: url,
					correctLevel : QRCode.CorrectLevel.H
				});
				$('#qrcode').append( "&nbsp;" );
				if(i%3==0 && i!=12){		//només si es de 200x200
				 		$('#qrcode').append( "<br>" );
				 		$('#qrcode').append( "<br>" );
				 		$('#qrcode').append( "<br>" );
				 	}
				}
		}
		else if($scope.mida==300){	//Els codis QR de 300x300
			if($scope.numQRs>6) {	//6 es el maxim per aquesta mida
				$scope.numQRs=6;
			}
			for(var i=1; i<= $scope.numQRs; i++){
				$('#qrcode').append( "&nbsp;" );
				$('#qrcode').append( "&nbsp;" );
				var qrcode = new QRCode(document.getElementById("qrcode"), {
					width : $scope.mida,
					height : $scope.mida,
					text: url,
					correctLevel : QRCode.CorrectLevel.H
				});
				$('#qrcode').append( "&nbsp;" );
				 if(i%2==0 && i!=6){	
				 	$('#qrcode').append( "<br>" );
				 	$('#qrcode').append( "<br>" );
				 }
			}
		}
		else if($scope.mida==400 || $scope.mida==450){	//Els codis QR de 400x400 o 450x450
			if($scope.numQRs>2) {	//2 es el maxim per aquesta mida
				$scope.numQRs=2;
			}
			for(var i=1; i<= $scope.numQRs; i++){
				$('#qrcode').append( "&nbsp;" );
				$('#qrcode').append( "&nbsp;" );
				var qrcode = new QRCode(document.getElementById("qrcode"), {
					width : $scope.mida,
					height : $scope.mida,
					text: url,
					correctLevel : QRCode.CorrectLevel.H
				});
				 $('#qrcode').append( "<br>" );
				 $('#qrcode').append( "<br>" );
			}
		}
	    window.print();		///Obrir la finestra del navegador per imprimir (seleccionar dispositiu, copies,etc...)
	}
}])