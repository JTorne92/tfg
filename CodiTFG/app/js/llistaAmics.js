'use strict';
 
angular.module('myApp.llistaAmics', ['ngRoute','firebase'])

.controller('llistaAmicsCtrl', ['$scope','$firebaseObject','$firebaseArray','$location','$modal','$timeout', function($scope,$firebaseObject,$firebaseArray,$location,$modal,$timeout){
	$scope.loading=true;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
  	$scope.userID= authData.uid;
  	var refUser= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID);
  	$scope.userSessio= $firebaseObject(refUser);
  	var refUsuarisAmics= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID + "/usuarisAmics");
  	$scope.usuarisAmics= $firebaseArray(refUsuarisAmics);
  	///Comprova que tots existeixen
  	$scope.IDsusuarisAmics=[];
  	function buscarIdUsuari(username,i){
  		var refuserExisteix = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+username);
  		refuserExisteix.on("value", function(snapshot) {
				    var usuariComprovar= snapshot.val();
				    if(usuariComprovar != null){
				    	$scope.IDsusuarisAmics[$scope.IDsusuarisAmics.length] = usuariComprovar.userIDassociat;	//Agafem l'ID per poder accedir al perfil posteriorment
				    	return;
				   	}
				   	else{
				   		$scope.usuarisAmics.$remove(i)	///Per si l'usuari ha esborrat la seva compta.
				   		return;
				   	}
			}, function (errorObject) {
			 	console.log("The read failed: " + errorObject.code);
			 	return;
			});
  	}
  	$scope.usuarisAmics.$loaded().then(function() {		///Carguem la llista d'usuaris amics
  		for(var i=0; i< $scope.usuarisAmics.length; i++){
  			buscarIdUsuari($scope.usuarisAmics[i].username,i);
  		}
	})
  	$scope.afegirUsuari= function(nomUsuari){
  		$scope.afgError = false;
  		if(nomUsuari== "" || nomUsuari== null){		//Comprovem que s'hagi introduit alguna cosa
  			$timeout(function() {
          		$scope.afgError = true;
          		$scope.afgErrorMessage ="Introdueix un nom d'usuari per afegir.";
        	});
    	}
    	else{
    			var refusersReg = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+ nomUsuari);
			    refusersReg.on("value", function(snapshot) {
				    var usuariExistent= snapshot.val();
				    if(usuariExistent != null){		///Comprovem que l'usuari introduit existeix
				    	if(usuariExistent.userIDassociat == $scope.userID){	//Comprovem que no siguis tu mateix
				    		$timeout(function() {
				               	$scope.afgError = true;	
				                $scope.afgErrorMessage ="L'usuari introduit no pots ser tu.";
				           	});
				        }
				     	else{
				     		var posUs= $scope.usuarisAmics.$indexFor(nomUsuari);	
				     		if(posUs!= -1){		///Comprovem que no formi part de la llista
	    						$scope.afgError = true;
	          					$scope.afgErrorMessage ="Aquest usuari ja las afegit com amic.";
	    					}
	    					else{
								$timeout(function() {
	                  				$scope.afgError = false;		//Si s'ha arribat fins aqui l'usuari és correcte i s'afegeix
	                  				refUsuarisAmics.child(nomUsuari).set({
										username : nomUsuari
									});
	                			});
	    					}
				     			
				     	}
				   	}  
				   	else{
				       	$timeout(function() {
				           	$scope.afgError = true;
				           	$scope.afgErrorMessage ="L'usuari no existeix.";
				       	});
				   	}
			 	}, function (errorObject) {
			      	console.log("The read failed: " + errorObject.code);
			 	});
		}	
  	};
  	$scope.open = function (ind) {
	    var modalInstance = $modal.open({
	        templateUrl: 'EliminarUserAmicContent.html',
	        controller: 'EliminarUserAmicInstanceCtrl',
	    })
	    modalInstance.result.then(function () {
	    	$scope.usuarisAmics.$remove(ind)
	    }, function () {
	      console.log("Model dismiss");
	    });
	    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
	};
	$scope.veurePerfil= function(ind){
		$location.path("/perfil/"+ $scope.IDsusuarisAmics[ind]); 
	}
	$scope.loading=false;
}])
.controller('EliminarUserAmicInstanceCtrl', ['$scope','$modalInstance', function ($scope, $modalInstance) {
  	$scope.ok = function () { 
      	$modalInstance.close();
  	};
  	$scope.cancel = function () {
    	$modalInstance.dismiss('cancel');
  	};
}])
