'use strict';
 
angular.module('myApp.modificarItem', ['ngRoute','firebase'])

.controller('modificarItemCtrl', ['$scope','$routeParams','$location','$firebaseObject','$firebaseArray', '$modal', function($scope,$routeParams,$location,$firebaseObject,$firebaseArray, $modal){
	$scope.loading=true;
  $scope.catid =  $routeParams.catid;
	$scope.userID = $routeParams.userID;
	$scope.itemid =  $routeParams.itemid;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
  	$scope.propietari=false;
  	if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
    	$scope.propietari=true;
  	}
  	else{                          //Si no ets el propietari, et porta a l'inici
   	 	console.log("No ets el propietari")
    	$location.path("/");
  	}
	var refCatNom = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid+"/nom");
	$scope.nomCat=$firebaseObject(refCatNom);
	var refItem= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/" + $scope.itemid); 
	$scope.item= $firebaseObject(refItem);
  $scope.existeix=true;
	$scope.modifciacioNom= "";
	$scope.item.$loaded().then(function() {
		$scope.modifciacioNom= $scope.item.nom;
    if($scope.item.nom==null){
      $scope.existeix=false;  //Comprovar si existeix el ítem
    }
		switch($scope.item.acces) {
		    case "privat":
		    	$scope.tipusAcces= "Privat";
		    	$scope.accesTipus="privat";
		        break;
		    case "public":
		    	$scope.tipusAcces= "Públic"
		    	$scope.accesTipus="public";
		        break;
		    case "alguns":
		    	$scope.tipusAcces= "Restringit per algun usuaris"
		    	$scope.accesTipus="alguns";
		        break;
		} 
	})
	var refAtributsOld= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/" + $scope.itemid +"/Atributs");
	$scope.atributsModificar= []; 
	var oldAtributs= $firebaseArray(refAtributsOld); 
	oldAtributs.$loaded().then(function() {  ///Agafem els atributs de la BD
		var arrayLengtholdAtributs = oldAtributs.length;
		for (var k = 0; k < arrayLengtholdAtributs; k++) {
			var atributIteracio= {
				nom : oldAtributs[k].nom,
				contingut: oldAtributs[k].contingut
			}
			$scope.atributsModificar.push(atributIteracio);  //Es crea una altre array perquè fins que no confirmem no podem fer els canvis a la BD
		}
	})
	$scope.usuarisAccesDef =[] 
	var refAccesOld= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/" + $scope.itemid +"/usuarisAcces");
	$scope.usuarisAccesDef= []; 
	var oldUsers= $firebaseArray(refAccesOld); ///Agafem els usuaris que tenen accés a l'ítem de la BD
	oldUsers.$loaded().then(function() { 
		var arrayLengtholdUsers = oldUsers.length;
		for (var k = 0; k < arrayLengtholdUsers; k++) {
			$scope.usuarisAccesDef.push(oldUsers[k].nom)    //Es crea una altre array perquè fins que no confirmem no podem fer els canvis a la BD
		}
	})
	$scope.afegirAtribut= function(){
		$scope.atributsModificar.push({nom: "", contingut: ""});
	}
	$scope.esborrarAtribut= function(index){
		$scope.atributsModificar.splice(index,1);
	}
	$scope.atributUp= function(index){
		var aux = $scope.atributsModificar[index];
		$scope.atributsModificar[index] = $scope.atributsModificar[index-1];
		$scope.atributsModificar[index-1] = aux;
	}
	$scope.atributDown= function(index){
		var aux = $scope.atributsModificar[index];
		$scope.atributsModificar[index] = $scope.atributsModificar[index+1];
		$scope.atributsModificar[index+1] = aux;
	}
	$scope.cancelar= function(){
		$location.path("/item/"+$scope.userID+"/"+$scope.catid +"/"+ $scope.itemid);
	}
	$scope.GuardarCanvis=function(){
		var nomItem= $scope.modifciacioNom;
		var tipusA= $scope.accesTipus;
		var today= new Date();
		var dia= today.getTime()
    //Actualizem les dades de la BD
		refItem.update({  
				nom: nomItem,
				DataModificacio: dia,
        		acces: tipusA
		})  
    ///Es borra tots els usuaris que tenien accés i es crea un altre cop els usuaris.
		refAccesOld.remove();
		var arrayLengthUsuaris = $scope.usuarisAccesDef.length;
		for (var j = 0; j < arrayLengthUsuaris; j++) {
		      var nomUsuari= $scope.usuarisAccesDef[j];
		      refAccesOld.child($scope.usuarisAccesDef[j]).set({
		        nom: nomUsuari
		    });
		}
		refAtributsOld.remove();
    ///Es borra tots els atributs que teniem i es crea un altre cop els atributs.
		var arrayLengthAtributs = $scope.atributsModificar.length;
		for (var i = 0; i < arrayLengthAtributs; i++) {
		    var nomAt= $scope.atributsModificar[i].nom;
		    var contAt= $scope.atributsModificar[i].contingut;
		    refAtributsOld.push({
		    	nom: nomAt,
		    	contingut: contAt
		    });
		}
    ///Un cop acabat tornem la pàgina de veure l'ítem.
		$location.path("/item/"+$scope.userID+"/"+$scope.catid +"/"+ $scope.itemid);
	}
	//////////// Carguem la llista d'usuaris amics per utilitzar-lo per si volem modificar l'accés
  var refusersAmics= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/usuarisAmics");
  $scope.usersAmics= $firebaseArray(refusersAmics);
	$scope.open = function () {
    $scope.aUsers= $scope.usuarisAccesDef.slice(); 
      var modalInstance = $modal.open({
        templateUrl: 'ModAccesItemContent.html',
        controller: 'ModAccesItemInstanceCtrl',
        resolve: {
          aUsers: function(){
            return $scope.aUsers;
          },
          usuarisAccesDef: function(){
            return $scope.usuarisAccesDef;
          }, 
          accesTipus: function(){
            return $scope.accesTipus;
          },
          userID: function(){
            return $scope.userID;
          },
          userAmics: function(){
            return $scope.usersAmics;
          }
        }
    });
    modalInstance.result.then(function (resultat) {
    $scope.usuarisAccesDef=resultat.usus.slice(); 
    $scope.accesTipus=resultat.acces; 
    	switch($scope.accesTipus) {
		    case "privat":
		    	$scope.tipusAcces= "Privat";
		        break;
		    case "public":
		    	$scope.tipusAcces= "Públic"
		        break;
		    case "alguns":
		    	$scope.tipusAcces= "Restringit per algun usuaris"
		        break;
		} 
    }, function () {
      console.log("Model dismiss");
    });
    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      if (modalInstance) {
        modalInstance.dismiss('cancel');
      }
    });
  };
  $scope.loading=false;
}])

.controller('ModAccesItemInstanceCtrl', ['$scope','$modalInstance','aUsers','usuarisAccesDef','accesTipus', 'userID', '$timeout', 'userAmics', function ($scope, $modalInstance,aUsers,usuarisAccesDef,accesTipus, userID, $timeout, userAmics) {
  $scope.aUsers=aUsers;
  $scope.accesTipus= accesTipus;
  $scope.usuarisAccesDef= usuarisAccesDef;
  $scope.userID= userID;
  $scope.usersAmics= userAmics;
  $scope.ok = function () { 
  	  var resultatConf= {
  	  	usus: $scope.aUsers,
  	  	acces: $scope.accesTipus
  	  }
      $modalInstance.close(resultatConf);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  $scope.escollitAmic= function(amic){
    $scope.usernameAfegir= amic;  
  }
  $scope.afegirUsuari= function(nomUsuari){
    $scope.afgError = false;
    if(nomUsuari== "" || nomUsuari== null){
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="Introdueix un nom d'usuari per afegir.";
        });
    }
    else{ 
      if($scope.aUsers.indexOf(nomUsuari)== -1){
        //Comprova si està a la llista
        var refusers = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+nomUsuari);
        refusers.on("value", function(snapshot) {
          var usuariExistent= snapshot.val();
          if(usuariExistent != null){
            //Comprovar si existeix
            if(usuariExistent.userIDassociat != $scope.userID){
                //Comprovar si ets tu
                $timeout(function() {
                  $scope.aUsers.push(nomUsuari);
                  $scope.usernameAfegir= "";
                });
            }
            else{
                $timeout(function() {
                  $scope.afgError = true;
                  $scope.afgErrorMessage ="L'usuari introduit no pots ser tu.";
                });
                console.log("L'usuari introduit no pots ser tu")
            }
          }  
          else{
            $timeout(function() {
              $scope.afgError = true;
              $scope.afgErrorMessage ="L'usuari no existeix.";
            });
            console.log("L'usuari no existeix")
          }
        }, function (errorObject) {
          console.log("The read failed: " + errorObject.code);
        });
      }
      else{
        console.log("L'usuari ja està en la llista")
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="L'usuari ja està en la llista.";
        });  
      }
    }
  	
  };
  $scope.eliminarUsuari= function(index){
      $scope.aUsers.splice(index, 1);
  }
}])