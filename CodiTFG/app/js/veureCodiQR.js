'use strict';
 
angular.module('myApp.veureCodiQR', ['ngRoute'])

.controller('veureCodiQRCtrl',['$scope','$routeParams','$location','$firebaseObject','$timeout', function($scope,$routeParams,$location,$firebaseObject,$timeout){
	$scope.loading=true;
	$scope.catid =  $routeParams.catid;
	$scope.userID = $routeParams.userID;
	$scope.itemid =  $routeParams.itemid;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
	$scope.propietari=false;
  	if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
    	$scope.propietari=true;
  	}
  	else{
   	 	console.log("No ets el propietari")
    	$location.path("/");
  	}
  	////Creem la URL que ha de tenir l'ítem
	var url= ("https://tfgcodisqr.firebaseapp.com/#/item/"+ $scope.userID + "/" + $scope.catid + "/" + $scope.itemid);
	$scope.existeix=true;
	var refItem = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/"+ $scope.itemid);
	var item= $firebaseObject(refItem);
	item.$loaded().then(function() {	//Comprovar que existeix el ítem
		if(item.nom == null){
			$timeout(function() {
				$scope.existeix=false; 
			})
		}
	})
	///Creem el codi QR, de mida 200x200
	var qrcode = new QRCode(document.getElementById("qrcode"), {
			width : 200,
			height : 200,
			text: url,
			correctLevel : QRCode.CorrectLevel.H
	});
	/////Les diferents mides que podem triar que tingui el codi QR
	$scope.optionsMida = [
    	{ label: 'Molt Petit', value: 100 },
    	{ label: 'Petit', value: 200 },
    	{ label: 'Mitjà', value: 300 },
    	{ label: 'Gran', value: 400 },
    	{ label: 'Molt Gran', value: 450 }
 	];
 	$scope.selectorDeMida = $scope.optionsMida[1]; //Per defecte la mida es 200x200 (Petit)
 	///Si canviem la mida, eliminia(buida el <div>) el QR anterior i crea el nou
 	$scope.update= function(){
 		$('#qrcode').empty();
 		var mida= $scope.selectorDeMida.value;
 		var qrcode = new QRCode(document.getElementById("qrcode"), {
			width : mida,
			height : mida,
			text: url,
			correctLevel : QRCode.CorrectLevel.H
		});
 	}
 	$scope.maxError=false;
 	$scope.numQRs=1;
 	$scope.afegir=function(){
 		$scope.numQRs+=1;
 	}
 	$scope.restar=function(){
 		$scope.numQRs-=1;
 	}
 	$scope.comprovacions=function(){
 		///Comprovar el màxims de codis QR que es poden fer en una pàgina segons la mida
 		if(($scope.selectorDeMida==$scope.optionsMida[0]) && $scope.numQRs >= 35){
 			$scope.maxError=true;
 			$scope.maxErrorMessage="El màxim de codis QR per pàgina amb aquesta mida és 35"
 			$scope.numQRs = 35; //per si abans era un mida més petita
 		}
 		else if(($scope.selectorDeMida==$scope.optionsMida[1]) && $scope.numQRs >= 12){
 			$scope.maxError=true;
 			$scope.maxErrorMessage="El màxim de codis QR per pàgina amb aquesta mida és 12"
 			$scope.numQRs = 12; //per si abans era un mida més petita
 		}
 		else if(($scope.selectorDeMida==$scope.optionsMida[2]) && $scope.numQRs >= 6){
 			$scope.maxError=true;
 			$scope.maxErrorMessage="El màxim de codis QR per pàgina amb aquesta mida és 6"
 			$scope.numQRs = 6; //per si abans era un mida més petita
 		}
 		else if((($scope.selectorDeMida==$scope.optionsMida[3]) || ($scope.selectorDeMida==$scope.optionsMida[4]))&& $scope.numQRs >= 2){
 			$scope.maxError=true;
 			$scope.maxErrorMessage="El màxim de codis QR per pàgina amb aquesta mida és 6"
 			$scope.numQRs = 2; //per si abans era un mida més petita
 		}
 		else{
 			$scope.maxError=false;
 		}
 	}
 	$scope.loading=false;
}])

