'use strict';
 
angular.module('myApp.veureItem', ['ngRoute','firebase'])

.controller('veureItemCtrl', ['$scope','$routeParams','$location','$firebaseObject','$firebaseArray', '$modal', function($scope,$routeParams,$location,$firebaseObject,$firebaseArray, $modal){
	$scope.loading=true;
	var ref= new Firebase("https://tfgcodisqr.firebaseio.com");
	var authData = ref.getAuth();
	$scope.catid =  $routeParams.catid;
	$scope.userID = $routeParams.userID;
	$scope.itemid =  $routeParams.itemid;
	var refusername = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/username");
	$scope.usernameCreador= $firebaseObject(refusername);
	var refNomCat = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/nom");
	$scope.nomCataleg=  $firebaseObject(refNomCat);
	var refItem = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/"+ $scope.itemid);
	$scope.Dadesitem = $firebaseObject(refItem);
	var refAtributs = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/"+ $scope.itemid +"/Atributs");
	$scope.atributs= $firebaseArray(refAtributs);
  	if(authData){ ////Comprovem si l'usuari esta loggejat
  		$scope.userIDSessio= authData.uid;
		var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
  		$scope.userSessio= $firebaseObject(refUserSessio);
  	}
  	else{
  		$scope.userSessio= "anonim"
  		$scope.userIDSessio= "anonim"
  	}
  	$scope.permis=false;		//S'ha de controlar l'acces, fins que no s'ha demostrat que tens acces no pots veure el contingut
  	$scope.propietari=false;
  	if(angular.equals($scope.userIDSessio, $scope.userID)){		///Comprovem si és el propietari
  		$scope.permis=true;	
  		$scope.propietari=true;
  	}
  	$scope.existeix=true;
  	var refConfUser= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID + "/acces");
	var refUsersAcces= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/"+ $scope.itemid +"/usuarisAcces/");
  	$scope.Dadesitem.$loaded().then(function() {
		$scope.accesItem= $scope.Dadesitem.acces;
		if($scope.accesItem==null){
			$scope.existeix=false;	//Comprovar si existeix el ítem
		}
		switch($scope.accesItem) {
		    case "privat":
		    	$scope.tipusAcces= "Privat"
		        console.log("Es privat");
		        if(!($scope.permis)){
		        	console.log("No tens accés")
		        	$scope.tipusPermis= "privat";
		        }
		        break;
		    case "public":
		    	$scope.tipusAcces= "Públic"
		        console.log("Es public");
		        	$scope.permis=true;
		        break;
		    case "alguns":
		    	$scope.tipusAcces= "Restringit per alguns usuaris"
		        console.log("Es alguns");
		        if(!($scope.permis)){
		        	var usuarisAcces= $firebaseArray(refUsersAcces); 
		        	usuarisAcces.$loaded().then(function() {
		        		var res= usuarisAcces.$indexFor($scope.userSessio.username);
		        		if(res== -1){		//Si és -1 no vol dir que no esta a la llista
							if($scope.userSessio== "anonim"){
								console.log("No tens accés, però pots fer login")
								$scope.tipusPermis= "algunsPotLog";
							}
		        			else{
		        				console.log("No tens accés")
		        				$scope.tipusPermis= "algunsNoTu";
		        			}
		        		}
		        		else{ 	//Estas a la llista, llavors tens accés
		        			$scope.permis=true;
		        		}
		        	})
		        }
		} 
	})
  	$scope.noAcces= function(){
  		///Si no tens accés redirigim a una pàgina per fer login 
  		if($scope.tipusPermis=="privat") {
  			$location.path("/acces/ItemPrivat");
  		}
  		else if($scope.tipusPermis=="algunsPotLog") {
  			$location.path("/acces/ItemLogin");
  		}
  		else if($scope.tipusPermis=="algunsNoTu") {
  			$location.path("/acces/ItemLlista");	
  		}
  	}
	$scope.traduirData= function(d){
    	return new Date(d)
  	}
  	$scope.modificar= function(){
  		$location.path("/modifItem/"+$scope.userID+"/"+$scope.catid+"/"+$scope.itemid);
  	}
	$scope.open = function () {
	      var modalInstance = $modal.open({
	        templateUrl: 'VeureEliminarItemContent.html',
	        controller: 'VeureEliminarItemInstanceCtrl',
	        resolve: {
	        }
	    });
	    modalInstance.result.then(function () { 
	    	refItem.remove();
	    	$location.path("/catalegs/"+$scope.userID+"/"+$scope.catid);
	    }, function () {
	      console.log("Model dismiss");
	    });
	    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
 	};
 	$scope.loading=false;
}])


.controller('VeureEliminarItemInstanceCtrl',['$scope','$modalInstance', function ($scope, $modalInstance){
	$scope.ok = function () { 
	      $modalInstance.close();
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
}])