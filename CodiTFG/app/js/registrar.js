'use strict';

angular.module('myApp.registrar', ['ngRoute','firebase'])
 

.controller('RegistrarCtrl', ['$scope','$location', function($scope,$location) {
	$scope.loading=true;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	$scope.working= false;
    $scope.Registrar = function() {
    	$scope.working= true;
     	if (!$scope.FormRegistrar.$invalid) {
     		 var email = $scope.user.email;
        	 var password = $scope.user.password;
        	 var username = $scope.user.username;
        	var usuariLliure;
        	 (function(userID){
			    	var refusers = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs");
			    	refusers.child(userID).once('value', function(snapshot) {
			    	///Comrpovem si el username no esta agafat
			        if((snapshot.val() == null)) {
				        if (email && password) {
				        	///Creem l'usuari
			        	ref.createUser({
						  email    : email,
						  password : password
						}, function(error, userData) {
						  if (error) {
						  	$scope.regError = true;
						  	$scope.working= false;
						    switch (error.code) {
						      case "EMAIL_TAKEN":
						      	$scope.regErrorMessage ="L'email ja està agafat.";
						        console.log("The new user account cannot be created because the email is already in use.");
						        break;
						      case "INVALID_EMAIL":
						      	$scope.regErrorMessage ="L'email és invalid.";
						        console.log("The specified email is not a valid email.");
						        break;
						      default:
						        console.log("Error creating user:", error);
						    }
						    $scope.$apply();
						  } else {
						  	$scope.regError = false;
						    console.log("Successfully created user account with uid:", userData.uid);
						    ref.authWithPassword({
							  "email": email,
							  "password": password
							}, function(error, authData) {
								  if (error) {
								    console.log("Login Failed!", error); 
								  } 
									else {
										var today= new Date();
										var dia= today.getTime()
								    	console.log("Authenticated successfully with payload:", authData);	
								    	///Creem les dades del usuari(per poder accedir al perfil)
							    		ref.child('users').child(authData.uid).set({
							    			email: email ,
										    username: username ,
										    DataCreat: dia
							    		});
							    		////L'afegim a la llista (que servei per controlar que no es repeteixin usernames i guardar els IDs)
							    		ref.child('uRegs').child(username).set({
							    			userIDassociat : authData.uid
							    		});
							    		///Creem l'accés al perfil de l'usuari, per defecte privat
							    		ref.child('confAccesPerfil').child(authData.uid).set({
							    			acces : "privat"
							    		})
								    	$scope.$apply(function() { $location.path("/listCat"); });	///Entrem al sistema i mostrem la llista de catàlegs
								    }
								});
						  	}
						})
						}
			        }
			        else{
			        	console.log("Username ja està agafat");
			        	$scope.regError = true;
			        	$scope.regErrorMessage ="El nom d'usuari ja està agafat.";
			        	$scope.working= false;
			        	$scope.$apply();
			        }
			   		})
			   	})(username);
    	}
    };
    $scope.loading=false;
}])


.directive('passwordVerify', function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
            }                    
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
 	}
  })

