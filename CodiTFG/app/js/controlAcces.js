'use strict';
 
angular.module('myApp.controlAcces', ['ngRoute','firebase'])

.controller('controlAccesCtrl', ['$scope','$routeParams','$firebaseObject','$location', function($scope,$routeParams,$firebaseObject,$location){
	$scope.loading=true;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	var authData = ref.getAuth();
  	if(authData){			//Es comprova si l'usuari esta loggejat
  		$scope.userIDSessio= authData.uid;
		var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
  		$scope.userSessio= $firebaseObject(refUserSessio);
  	}
  	else{					//Si es un usuari no loggejat, serà anonim
  		$scope.userSessio= "anonim"
  		$scope.userIDSessio= "anonim"
  	}
  	$scope.loginOut= function(){
	    ref.unauth();
	    $location.path("/");
	}
	$scope.tipus= $routeParams.tipus;	//Per saber el tipus de missatge per mostrar
	$scope.working= false;				//Per deshabilitar el botó si està processant, per evitar clics de més
	$scope.SignIn = function(enter) {
		$scope.working= true;
		ref.unauth();					//Tanca la sessió de l'usuari per entrar amb un altre
		enter.preventDefault();
	    var email = $scope.user.email;
	    var password = $scope.user.password;
		ref.authWithPassword({
		  "email": email,
		  "password": password
		}, function(error, authData) {
		  if (error) {
		    $scope.LogError = true;
		    $scope.working= false;
		    switch (error.code) {
		      case "INVALID_EMAIL":
		        $scope.regErrorMessage ="L'email és invalid.";
		        break;
		      case "INVALID_PASSWORD":
		        $scope.regErrorMessage ="La contrasenya és incorrecta.";
		        break;
		      case "INVALID_USER":
		        $scope.regErrorMessage ="L'usuari no existeix.";
		        break;
		      default:
		        $scope.regErrorMessage ="Error al entrar al sistema.";
		    }
		    $scope.$apply();
		  } else {
		    console.log("Authenticated successfully with payload:", authData);
		    $scope.LogError = false;
		    window.history.back();
		  }
		});
	}
	$scope.loading=false;
}])