'use strict';
 
angular.module('myApp.modificarPerfil', ['ngRoute','firebase'])

.controller('modificarPerfilCtrl', ['$scope','$routeParams','$firebaseObject','$timeout','$modal', '$location', '$firebaseArray', function($scope,$routeParams,$firebaseObject,$timeout,$modal, $location, $firebaseArray){
	$scope.loading=true;
	$scope.userID = $routeParams.userID;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
  	if(!authData){  //No esta loggejat
    	$location.path("/");
  	}
  	$scope.propietari=false;
  	if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
    	$scope.propietari=true;
  	}
  	else{
   	 	console.log("No ets el propietari")
    	$location.path("/");
  	}
	var refUser= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID);
	$scope.user= $firebaseObject(refUser);
	///Es crea el lloc on es guardaran les modificacions
	$scope.modificacionsUser= {
		nomComplet: "",
		email: "",
		Oldpassword: "",
		Noupassword: "",
		Confpassword: ""
	};
	///Agafem de la BD el nom i l'email
	$scope.user.$loaded().then(function() {
		$scope.modificacionsUser.nomComplet= $scope.user.nom;
		$scope.modificacionsUser.email= $scope.user.email;
	})
	$scope.FormPass=false;
	var refConfUser= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID + "/acces");
	refConfUser.on("value", function(snapshot) {
	  	switch(snapshot.val()) {
		    case "privat":
		    	$scope.tipusAcces= "Privat";
		    	$scope.accesTipus="privat";
		        break;
		    case "public":
		    	$scope.tipusAcces= "Públic"
		    	$scope.accesTipus="public";
		        break;
		    case "alguns":
		    	$scope.tipusAcces= "Restringit per algun usuaris"
		    	$scope.accesTipus="alguns";
		        break;
		} 
	}, function (errorObject) {
	  console.log("The read failed: " + errorObject.code);
	});
	var refAccesOld= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID + "/usuarisAcces");
	$scope.usuarisAccesDef= []; 
	var oldUsers= $firebaseArray(refAccesOld);
	oldUsers.$loaded().then(function() {
		var arrayLengtholdUsers = oldUsers.length;
		for (var k = 0; k < arrayLengtholdUsers; k++) {
			$scope.usuarisAccesDef.push(oldUsers[k].nom)
		}
	})
	$scope.traduirData= function(d){
    	return new Date(d)
  	}
  	$scope.MostrarFormPass=function(){	//Per si volem canviar la contrasenya
  		$scope.FormPass=true;
  	}
  	$scope.OcultarFormPass=function(){	//Per si decidim que ja no volem canviar la contrasenya
  		$scope.FormPass=false;
  		$scope.modificacionsUser.Oldpassword="";
  		$scope.modificacionsUser.Noupassword="";
  		$scope.modificacionsUser.Confpassword="";
  	}
  	$scope.cancelar= function(){
  		$location.path("/perfil/"+$scope.userID); 
  	}
  	$scope.GuardarCanvis=function(){
  		function GuardarDades(){
			var nomComplet= $scope.modificacionsUser.nomComplet;
			if((nomComplet!= null) && (nomComplet!="")){
				refUser.update({
				  	"nom": nomComplet
				});
			}
			var refAccesUser= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID);
			var tipAcces= $scope.accesTipus;
			refAccesUser.update({
				  "acces": tipAcces
			});
			refAccesOld.remove();
			var arrayLengthUsuaris = $scope.usuarisAccesDef.length;
		    for (var j = 0; j < arrayLengthUsuaris; j++) {
		      var nomUsuari= $scope.usuarisAccesDef[j];
		      refAccesUser.child('usuarisAcces').child($scope.usuarisAccesDef[j]).set({
		        nom: nomUsuari
		      });
		    }
		    $location.path("/perfil/"+$scope.userID); 
		}
  		$scope.passError = false
  		if($scope.FormPass){
	  		var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	  		var oldPassword= $scope.modificacionsUser.Oldpassword;
	  		var newPassword= $scope.modificacionsUser.Noupassword;
	  		var email= $scope.modificacionsUser.email
			ref.changePassword({
			  email       : email,
			  oldPassword : oldPassword,
			  newPassword : newPassword
			}, function(error) {
			  if (error === null) {
			    console.log("Password changed successfully");
			    GuardarDades();
			  } else {
			    console.log("Error changing password:", error);
			    $timeout(function() {
		          	$scope.passError= true;
			      	$scope.passErrorMessage ="La contrasenya antiga no és correcte.";
		       });
			  }
			});
		}
		if(!($scope.FormPass)){
			GuardarDades();
		}
	}
	////////////////////
	var refusersAmics= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/usuarisAmics");
	$scope.usersAmics= $firebaseArray(refusersAmics);
	$scope.open = function () {
	    $scope.aUsers= $scope.usuarisAccesDef.slice(); 
	      var modalInstance = $modal.open({
	        templateUrl: 'AccesPerfilContent.html',
	        controller: 'AccesPerfilInstanceCtrl',
	        resolve: {
	          aUsers: function(){
	            return $scope.aUsers;
	          },
	          usuarisAccesDef: function(){
	            return $scope.usuarisAccesDef;
	          }, 
	          accesTipus: function(){
	            return $scope.accesTipus;
	          },
	          userID: function(){
	            return $scope.userID;
	          },
	          userAmics: function(){
	          	return $scope.usersAmics;
	          }
	        }
	    });
	    modalInstance.result.then(function (resultat) {
	    $scope.usuarisAccesDef=resultat.usus.slice(); 
	    $scope.accesTipus=resultat.acces; 
		    switch($scope.accesTipus) {
			    case "privat":
			    	$scope.tipusAcces= "Privat";
			        break;
			    case "public":
			    	$scope.tipusAcces= "Públic"
			        break;
			    case "alguns":
			    	$scope.tipusAcces= "Restringit per algun usuaris"
			        break;
			} 
	    }, function () {
	      console.log("Model dismiss");
	    });
	    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
	};
	$scope.loading=false;
}])


.controller('AccesPerfilInstanceCtrl', ['$scope','$modalInstance','aUsers','usuarisAccesDef','accesTipus', 'userID', '$timeout', 'userAmics', function ($scope, $modalInstance,aUsers,usuarisAccesDef,accesTipus, userID, $timeout, userAmics) {
  $scope.aUsers=aUsers;
  $scope.accesTipus= accesTipus;
  $scope.usuarisAccesDef= usuarisAccesDef;
  $scope.userID= userID;
  $scope.usersAmics= userAmics;
  $scope.ok = function () { 
  	  var resultatConf= {
  	  	usus: $scope.aUsers,
  	  	acces: $scope.accesTipus
  	  }
      $modalInstance.close(resultatConf);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  $scope.escollitAmic= function(amic){
  	$scope.usernameAfegir= amic;	
  }
  $scope.afegirUsuari= function(nomUsuari){
    $scope.afgError = false;
    if(nomUsuari== "" || nomUsuari== null){
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="Introdueix un nom d'usuari per afegir.";
        });
    }
    else{ 
      if($scope.aUsers.indexOf(nomUsuari)== -1){
        //Comprova si està a la llista
        var refusers = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+nomUsuari);
        refusers.on("value", function(snapshot) {
          var usuariExistent= snapshot.val();
          if(usuariExistent != null){
            //Comprovar si existeix
            if(usuariExistent.userIDassociat != $scope.userID){
                //Comprovar si ets tu
                $timeout(function() {
                  $scope.aUsers.push(nomUsuari);
                  $scope.usernameAfegir= "";
                });
            }
            else{
                $timeout(function() {
                  $scope.afgError = true;
                  $scope.afgErrorMessage ="L'usuari introduit no pots ser tu.";
                });
                console.log("L'usuari introduit no pots ser tu")
            }
          }  
          else{
            $timeout(function() {
              $scope.afgError = true;
              $scope.afgErrorMessage ="L'usuari no existeix.";
            });
            console.log("L'usuari no existeix")
          }
        }, function (errorObject) {
          console.log("The read failed: " + errorObject.code);
        });
      }
      else{
        console.log("L'usuari ja està en la llista")
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="L'usuari ja està en la llista.";
        });  
      }
    }
  	
  };
  $scope.eliminarUsuari= function(index){
      $scope.aUsers.splice(index, 1);
  }
}])
