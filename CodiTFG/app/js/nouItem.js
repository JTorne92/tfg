'use strict';
 
angular.module('myApp.nouItem', ['ngRoute','firebase'])

.controller('nouItemCtrl', ['$scope','$routeParams','$location','$firebaseObject', '$modal', '$firebaseArray', function($scope,$routeParams,$location,$firebaseObject, $modal, $firebaseArray){
  $scope.loading=true;
  $scope.catid =  $routeParams.catid;
  $scope.userID = $routeParams.userID;
  var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  var authData = ref.getAuth();
  if(!authData){  //No esta loggejat
    $location.path("/");
  }
  $scope.propietari=false;
  if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
      $scope.propietari=true;
  }
  else{
    console.log("No ets el propietari")
    $location.path("/");
  }
	$scope.atributsCreats= [{}];
	var refCatNom = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid+"/nom");
	$scope.nomCat=$firebaseObject(refCatNom);
  $scope.existeix=true;
  $scope.nomCat.$loaded().then(function() {
    if($scope.nomCat.$value==null){  ///Comprovar que aquest catàleg existeix
      $scope.existeix=false;
    }
  })
	$scope.afegirAtribut= function(){
		$scope.atributsCreats.push({nom: "", contingut: ""});
	}
	$scope.esborrarAtribut= function(index){
		$scope.atributsCreats.splice(index,1);
	}
  $scope.atributUp= function(index){
    var aux = $scope.atributsCreats[index];
    $scope.atributsCreats[index] = $scope.atributsCreats[index-1];
    $scope.atributsCreats[index-1] = aux;
  }
  $scope.atributDown= function(index){
    var aux = $scope.atributsCreats[index];
    $scope.atributsCreats[index] = $scope.atributsCreats[index+1];
    $scope.atributsCreats[index+1] = aux;
  }
	$scope.usuarisAccesDef= [];
 	$scope.accesTipus = "privat"; //Per defecte és privat
	$scope.crearItem= function(){
		var refCat = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid);
		var nomDelItem= $scope.nomItem;
		var today= new Date();
		var dia= today.getTime()
    var tipAcces= $scope.accesTipus;
    ///Afegim l'item a la BD
		var itemAfegit= refCat.child('Items').push({
				nom: nomDelItem,
				DataCreacio: dia,
				DataModificacio: dia,
        acces: tipAcces
		});
		var itemID = itemAfegit.key();
		var refAtr= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items/" + itemID); 
		var arrayLength = $scope.atributsCreats.length;
    ///Afegim tots els atributs a la BD
		for (var i = 0; i < arrayLength; i++) {
		    var nomAt= $scope.atributsCreats[i].nom;
		    var contAt= $scope.atributsCreats[i].contingut;
		    refAtr.child('Atributs').push({
		    	nom: nomAt,
		    	contingut: contAt
		    });
		}
    var arrayLengthUsuaris = $scope.usuarisAccesDef.length;
    for (var j = 0; j < arrayLengthUsuaris; j++) {
      var nomUsuari= $scope.usuarisAccesDef[j];
      refAtr.child('usuarisAcces').child($scope.usuarisAccesDef[j]).set({
        nom: nomUsuari
      });
    }
		$location.path("/catalegs/"+$scope.userID+"/"+$scope.catid); 
	}
	$scope.cancelar= function(){
		$location.path("/catalegs/"+$scope.userID+"/"+$scope.catid); 
	}
  var refusersAmics= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/usuarisAmics");
  $scope.usersAmics= $firebaseArray(refusersAmics);
  $scope.open = function () {
    $scope.aUsers= $scope.usuarisAccesDef.slice(); 
      var modalInstance = $modal.open({
        templateUrl: 'CrearAccesItemContent.html',
        controller: 'CrearAccesItemInstanceCtrl',
        resolve: {
          aUsers: function(){
            return $scope.aUsers;
          },
          usuarisAccesDef: function(){
            return $scope.usuarisAccesDef;
          }, 
          accesTipus: function(){
            return $scope.accesTipus;
          },
          userID: function(){
            return $scope.userID;
          },
          userAmics: function(){
            return $scope.usersAmics;
          }
        }
    });
    modalInstance.result.then(function (resultat) {
    $scope.usuarisAccesDef=resultat.usus.slice(); 
    $scope.accesTipus=resultat.acces; 
    }, function () {
      console.log("Model dismiss");
    });
    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
        if (modalInstance) {
            modalInstance.dismiss('cancel');
        }
    });
  };
  $scope.loading=false;
}])

.controller('CrearAccesItemInstanceCtrl',['$scope','$modalInstance','aUsers','usuarisAccesDef','accesTipus', 'userID', '$timeout', 'userAmics', function ($scope, $modalInstance,aUsers,usuarisAccesDef,accesTipus, userID, $timeout, userAmics) {
  $scope.aUsers=aUsers;
  $scope.accesTipus= accesTipus;
  $scope.usuarisAccesDef= usuarisAccesDef;
  $scope.userID= userID;
  $scope.usersAmics= userAmics;
  $scope.ok = function () { 
  	  var resultatConf= {
  	  	usus: $scope.aUsers,
  	  	acces: $scope.accesTipus
  	  }
      $modalInstance.close(resultatConf);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  $scope.escollitAmic= function(amic){
    $scope.usernameAfegir= amic;  
  }
  $scope.afegirUsuari= function(nomUsuari){
    $scope.afgError = false;
    if(nomUsuari== "" || nomUsuari== null){
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="Introdueix un nom d'usuari per afegir.";
        });
    }
    else{ 
      if($scope.aUsers.indexOf(nomUsuari)== -1){
        //Comprova si està a la llista
        var refusers = new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+nomUsuari);
        refusers.on("value", function(snapshot) {
          var usuariExistent= snapshot.val();
          if(usuariExistent != null){
            //Comprovar si existeix
            if(usuariExistent.userIDassociat != $scope.userID){
                //Comprovar si ets tu
                $timeout(function() {
                  $scope.aUsers.push(nomUsuari);
                  $scope.usernameAfegir= "";
                });
            }
            else{
                $timeout(function() {
                  $scope.afgError = true;
                  $scope.afgErrorMessage ="L'usuari introduit no pots ser tu.";
                });
                console.log("L'usuari introduit no pots ser tu")
            }
          }  
          else{
            $timeout(function() {
              $scope.afgError = true;
              $scope.afgErrorMessage ="L'usuari no existeix.";
            });
            console.log("L'usuari no existeix")
          }
        }, function (errorObject) {
          console.log("The read failed: " + errorObject.code);
        });
      }
      else{
        console.log("L'usuari ja està en la llista")
        $timeout(function() {
          $scope.afgError = true;
          $scope.afgErrorMessage ="L'usuari ja està en la llista.";
        });  
      }
    }
  	
  };
  $scope.eliminarUsuari= function(index){
      $scope.aUsers.splice(index, 1);
  }
}])
 