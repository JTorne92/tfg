'use strict';
 
angular.module('myApp.listItems', ['ngRoute','firebase'])
 

.controller('listItemsCtrl', ['$scope','$routeParams','$firebaseArray','$location', '$modal', function($scope,$routeParams,$firebaseArray, $location,$modal) {
  $scope.loading=true;
  $scope.propietari=false;
  var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  var authData = ref.getAuth();
  if(!authData){  //No esta loggejat
    $location.path("/");
  }
	$scope.catid =  $routeParams.catid;
	$scope.userID = $routeParams.userID;
  if($scope.userID == authData.uid){ ///Comprovar que ets el propietari
    $scope.propietari=true;
  }
  else{
    console.log("No ets el propietari")
    $location.path("/");
  }
  $scope.existeix=true;
	var refItemsCat = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid + "/Items");
	var refCat = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID +"/Catalegs/"+ $scope.catid +"/nom");
	refCat.on("value", function(snapshot) {
  		$scope.nomCat= snapshot.val(); 
      if($scope.nomCat==null){  ///Comprovar que aquest catàleg existeix
        $scope.existeix=false;
      }
	}, function (errorObject) {
	  console.log("The read failed: " + errorObject.code);
	});
	$scope.items = $firebaseArray(refItemsCat);
  $scope.traduirData= function(d){
    return new Date(d)
  }
  $scope.ordreNomSeleccionat = "nom";
  $scope.ordenarPer = function(ordre) { //Funcio que permet ordenar els ítems
    if(ordre=="nom"){
      if($scope.ordreNomSeleccionat== "-nom"){
        $scope.ordreNomSeleccionat="nom"
      }
      else{
        $scope.ordreNomSeleccionat = "-nom";
      }
    }
    else if(ordre=="DataCreacio"){
      if($scope.ordreNomSeleccionat== "-DataCreacio"){
        $scope.ordreNomSeleccionat="DataCreacio"
      }
      else{
        $scope.ordreNomSeleccionat = "-DataCreacio";
      }
    }
    else if(ordre=="DataModificacio"){
      if($scope.ordreNomSeleccionat== "-DataModificacio"){
        $scope.ordreNomSeleccionat="DataModificacio"
      }
      else{
        $scope.ordreNomSeleccionat = "-DataModificacio";
      }
    }
  };
  $scope.open = function (index) {
    var modalInstance = $modal.open({
      templateUrl: 'EliminarItemContent.html',
      controller: 'EliItemInstanceCtrl',
      resolve: {
        items: function(){
          return $scope.items;
        },
        ind: function(){
          return index;
        }
      }
    });
    modalInstance.result.then(function () {
    }, function () {
      console.log("Model dismiss");
    });
      $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      if (modalInstance) {
        modalInstance.dismiss('cancel');
      }
    });
  };
  $scope.loading=false;
}])

.controller('EliItemInstanceCtrl', ['$scope','$modalInstance','items', 'ind', '$firebaseArray', function ($scope, $modalInstance, items, ind, $firebaseArray) {
  $scope.ok = function () {
      $scope.items= items;
      $scope.items.$remove(ind);  
      $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])
