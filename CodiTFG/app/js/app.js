'use strict';
 
angular.module('myApp', ['ngRoute',
		'myApp.inici',
		'myApp.registrar',
		'myApp.listCat',
		'myApp.listItems',
        'myApp.nouItem',
        'myApp.veureItem',
        'myApp.perfil',
        'myApp.veureCodiQR',
        'myApp.modificarPerfil',
        'myApp.modificarItem',
        'myApp.listUsuaris',
        'myApp.controlAcces',
        'myApp.barra',
        'myApp.llistaAmics',
        'myApp.vistaImprimir',
        'myApp.informacio',
        'myApp.listConverses',
        'myApp.veureConversa',
		"ui.bootstrap"])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/listCat', {
        templateUrl: 'views/listCat.html',
        controller: 'listCatCtrl'
    }).
    when('/registrar', {
        templateUrl: 'views/registrar.html',
        controller: 'RegistrarCtrl'
    }).
    when('/inici', {
        templateUrl: 'views/inici.html',
        controller: 'IniciCtrl'
    }).
    when('/catalegs/:userID/:catid', {
        templateUrl: 'views/listItems.html',
        controller: 'listItemsCtrl'
    }).
    when('/crearItem/:userID/:catid', {
        templateUrl: 'views/nouItem.html',
        controller: 'nouItemCtrl'
    }).
    when('/item/:userID/:catid/:itemid', {
        templateUrl: 'views/veureItem.html',
        controller: 'veureItemCtrl'
    }).
    when('/perfil/:userID', {
        templateUrl: 'views/perfil.html',
        controller: 'perfilCtrl'
    }).
    when('/veureQR/:userID/:catid/:itemid', {
        templateUrl: 'views/veureCodiQR.html',
        controller: 'veureCodiQRCtrl'
    }).
    when('/modifPerfil/:userID', {
        templateUrl: 'views/modificarPerfil.html',
        controller: 'modificarPerfilCtrl'
    }).
    when('/modifItem/:userID/:catid/:itemid', {
        templateUrl: 'views/modificarItem.html',
        controller: 'modificarItemCtrl'
    }).
    when('/listUsu', {
        templateUrl: 'views/listUsuaris.html',
        controller: 'listUsuarisCtrl'
    }).
    when('/acces/:tipus', {
        templateUrl: 'views/controlAcces.html',
        controller: 'controlAccesCtrl'
    }).
    when('/listAmics', {
        templateUrl: 'views/llistaAmics.html',
        controller: 'llistaAmicsCtrl'
    }).
    when('/print/:userID/:catid/:itemid/:sizeQR/:num', {
        templateUrl: 'views/vistaImprimir.html',
        controller: 'vistaImprimirCtrl'
    }).
    when('/informacio', {
        templateUrl: 'views/informacio.html',
        controller: 'informacioPCtrl'
    }).
    when('/listConv', {
        templateUrl: 'views/listConverses.html',
        controller: 'listConversesCtrl'
    }).
    when('/conversa/:IDConv', {
        templateUrl: 'views/veureConversa.html',
        controller: 'veureConversaCtrl'
    }).
    otherwise({
        redirectTo: '/inici'
    });

}]);