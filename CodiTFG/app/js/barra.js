'use strict';
 
angular.module('myApp.barra', ['ngRoute','firebase'])
 
 
.controller('barraCtrl', ['$scope','$firebaseObject', '$location','$firebaseArray', function($scope, $firebaseObject,$location,$firebaseArray) {
	$scope.loggejat=true; 
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
  	var authData = ref.getAuth();
	if(authData){  	//Es comprova si l'usuari esta loggejat
	  	$scope.loggejat=true;	
	  	$scope.userIDSessio= authData.uid;
		var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
  		$scope.userSessio= $firebaseObject(refUserSessio);
	}
	else{
	  	$scope.loggejat=false;	//Si no esta loggejat no s'ha de mostrar el botó d'usuari amb el perfil i login out.
	}
	$scope.loginOut= function(){
	    ref.unauth();
	    $location.path("/");
  	}
  	$scope.missatgesNous=false;
  	$scope.contador=0;
  	var refConversesTeves= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio +"/converses");
  	$scope.conversesTeves= $firebaseArray(refConversesTeves);
  	$scope.conversesTeves.$loaded().then(function() {
  		contar()
  		//Si hi ha un canvi en qualsevol de les nostres converses executa la següent funció
		$scope.conversesTeves.$watch(function() { 
			contar()
		});
  	})
  	function contar(){
  		$scope.contador=0;
	  	for (var i = 0; i < $scope.conversesTeves.length; i++) {
	  		//Mirem les converses en que tenim missatges sense llegir i incrementem el contador
	  		if($scope.conversesTeves[i].numNoVist>0) $scope.contador++;
	  	}
	  	if($scope.contador>0){
	  		$scope.missatgesNous=true;
	  	}
	  	else{
	  		$scope.missatgesNous=false;
	  	}
  	}
}])