'use strict';
 
angular.module('myApp.listUsuaris', ['ngRoute','firebase'])

.controller('listUsuarisCtrl', ['$scope','$routeParams','$firebaseObject','$firebaseArray', '$location', '$modal', function($scope,$routeParams,$firebaseObject,$firebaseArray, $location, $modal){
	$scope.loading=true;
	///Comprovar si es l'usuari administrador, si no s'en va al inici
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	var authData = ref.getAuth();
	if(!authData){	//Si no ha fet loggin, s'envia al inici
		$location.path("/");
	}
	$scope.userID= authData.uid;
	var refUserSessio = new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID);
	$scope.userSessio = $firebaseObject(refUserSessio);
	$scope.username="";
	$scope.userSessio.$loaded().then(function() {	
		$scope.username=$scope.userSessio.username;
		if($scope.username!="Administrador"){
			$location.path("/");
		}
	})
	///Agafar els usuaris
	var refUsers = new Firebase("https://tfgcodisqr.firebaseio.com/users/");
	$scope.usersSistema = $firebaseArray(refUsers);
	$scope.traduirData= function(d){
    	return new Date(d)
  	}
  	$scope.ordreNomSeleccionat = "username";
	$scope.ordenarPer = function(ordre) {
	    if(ordre=="username"){
	      if($scope.ordreNomSeleccionat== "-username"){
	        $scope.ordreNomSeleccionat="username"
	      }
	      else{
	        $scope.ordreNomSeleccionat = "-username";
	      }
	    }
	    else if(ordre=="DataCreat"){
	      if($scope.ordreNomSeleccionat== "-DataCreat"){
	        $scope.ordreNomSeleccionat="DataCreat"
	      }
	      else{
	        $scope.ordreNomSeleccionat = "-DataCreat";
	      }
	    }
	    else if(ordre=="email"){
	      if($scope.ordreNomSeleccionat== "-email"){
	        $scope.ordreNomSeleccionat="email"
	      }
	      else{
	        $scope.ordreNomSeleccionat = "-email";
	      }
	    }
	};
	$scope.open = function (username) {
	      var modalInstance = $modal.open({
	        templateUrl: 'ElimUsuariContent.html',
	        controller: 'ElimUsuariInstanceCtrl'
	    });
	    modalInstance.result.then(function () {
    		var refUsurReg= new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+ username );
    		$scope.userElim = $firebaseObject(refUsurReg);
			$scope.userElim.$loaded().then(function() {	
				$scope.userIDEliminar=$scope.userElim.userIDassociat;
				var refEliAccesUsuari= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userIDEliminar);
				var refEliUsurReg= new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+ username );
				var refEliUser= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDEliminar);
				$scope.conversesUsuari=  $firebaseArray(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDEliminar + "/converses"));
    			$scope.arrayConverseElim=[]
    			$scope.conversesUsuari.$loaded().then(function() {
	    			var numConvElim=0;
	    			function elimanarConversa(conv){
				  		var k = $scope.arrayConverseElim.indexOf(conv)
				  		if($scope.arrayConverseElim[k].contadorUsersActius<2){
				  			$scope.arrayConverseElim[k].$remove()
				  		}
				  		else{
				  			$scope.arrayConverseElim[k].contadorUsersActius= $scope.arrayConverseElim[k].contadorUsersActius-1;
							$scope.arrayConverseElim[k].$save()
				  		}
				  		numConvElim+=1;
				  		if(numConvElim==$scope.conversesUsuari.length){
				  			///Quan s'han tractat totes les nostres converses llavors podem eliminar l'usuari
				  			//Si no s'eliminava primer el usuari i amb l'usuari eliminat no podiem tractar les converses
				  			eliminarUsuari()
				  		}
				  	}
				  	function eliminarUsuari(){
				  		refEliAccesUsuari.remove();		//S'elimina totes les dades de la BD
	    				refEliUsurReg.remove();
	    				refEliUser.remove();
	    				/////No podem eliminar l'usuari en codi ja que no tenim la seva contrasenya. 
	    				//Només podem eliminar les dades de la BD, però continua havent un mail i contrasenya.
	    				/////Per eliminar completament l'administrador o ha de fer des de la web de Firebase.
				  	}
				  	if($scope.conversesUsuari.length>0){
		    			for (var i = 0; i < $scope.conversesUsuari.length; i++) {
		        			var IDConv= $scope.conversesUsuari[i].$id //ID de la conversa
		        			$scope.convEliminar= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ IDConv));
		        			$scope.arrayConverseElim.push($scope.convEliminar) 
		        			$scope.arrayConverseElim[i].$loaded().then(function(conv) {
						           elimanarConversa(conv)
						    })	        
		    			}
		    		}
		    		else{
		    			eliminarUsuari()
		    		}
	    		})
			})
	    }, function () {
	      console.log("Model dismiss");
	    });
	    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
  	};
  	$scope.loading=false;
}])

.controller('ElimUsuariInstanceCtrl', ['$scope','$modalInstance', function ($scope, $modalInstance) {
  $scope.ok = function () { 
	$modalInstance.close();
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
 }])