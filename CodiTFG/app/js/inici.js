'use strict';

angular.module('myApp.inici', ['ngRoute','firebase'])

.controller('IniciCtrl', ['$scope','$location', '$modal', function($scope,$location, $modal) {
	$scope.loading=true;
	var ref = new Firebase("https://tfgcodisqr.firebaseio.com");
	$scope.user={};
	var authData = ref.getAuth();
	if (authData) {		//Es comprova si l'usuari esta loggejat
    	$location.path("/listCat");	//Si esta loggejat, mostrem la llista de catàlegs
  	} 
  	$scope.working= false;
	$scope.SignIn = function(enter) {
		$scope.working= true;
		enter.preventDefault();
	    var email = $scope.user.email;
	    var password = $scope.user.password;
		ref.authWithPassword({
		  "email": email,
		  "password": password
		}, function(error, authData) {
		  if (error) {
		    $scope.LogError = true;
		    $scope.working= false;
		    switch (error.code) {
		      case "INVALID_EMAIL":
		        $scope.regErrorMessage ="L'email és invalid.";
		        break;
		      case "INVALID_PASSWORD":
		        $scope.regErrorMessage ="La contrasenya és incorrecta.";
		        break;
		      case "INVALID_USER":
		        $scope.regErrorMessage ="L'usuari no existeix.";
		        break;
		      default:
		        $scope.regErrorMessage ="Error al entrar al sistema.";
		    }
		    $scope.$apply();
		  } else {
		    console.log("Authenticated successfully with payload:", authData);
		    $scope.LogError = false;
		    $scope.$apply(function() { $location.path("/listCat"); });
		  }
		});
	}
	$scope.open = function () {
	      var modalInstance = $modal.open({
	        templateUrl: 'ForgPassContent.html',
	        controller: 'ForgPassInstanceCtrl',
	        resolve: {
	        }
	    });
	    modalInstance.result.then(function (fPass) { 
			ref.resetPassword({
    			email : fPass
  			}, function(error) {
  				if (error === null) {
    				console.log("Password reset email sent successfully");
  				} else {
    				console.log("Error sending password reset email:", error);
  				}
			});
	    }, function () {
	      console.log("Model dismiss");
	    });
	    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      		if (modalInstance) {
        		modalInstance.dismiss('cancel');
      		}
    	});
 	};
 	$scope.loading=false;
}])

.controller('ForgPassInstanceCtrl', ['$scope','$modalInstance', function ($scope, $modalInstance){
	$scope.ok = function (fPass) { 
		  console.log("El Mail es: " + fPass)
	      $modalInstance.close(fPass);
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
}])