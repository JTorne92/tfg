'use strict';
 
angular.module('myApp.perfil', ['ngRoute','firebase'])

.controller('perfilCtrl', ['$scope','$routeParams','$firebaseObject','$firebaseArray', '$location', '$modal', '$timeout', function($scope,$routeParams,$firebaseObject,$firebaseArray, $location, $modal, $timeout){
	$scope.loading=true;
	var ref= new Firebase("https://tfgcodisqr.firebaseio.com");
	var authData = ref.getAuth();
  	if(authData){		//Es comprova si l'usuari esta loggejat
  		$scope.userIDSessio= authData.uid;
		var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
  		$scope.userSessio= $firebaseObject(refUserSessio);
  	}
  	else{
  		$scope.userSessio= "anonim"
  		$scope.userIDSessio= "anonim"
  	}
  	$scope.userID = $routeParams.userID;
  	$scope.permis=false;		//S'ha de controlar l'acces, fins que no s'ha demostrat que tens acces no pots veure el contingut
  	$scope.propietari=false;
  	if(angular.equals($scope.userIDSessio, $scope.userID)){	
  		$scope.permis=true;
  		$scope.propietari=true;
  	}
	var refUser= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userID);
	$scope.user= $firebaseObject(refUser);
	///Es comprova si existeix el perfil
	$scope.existeix=true; 
	$scope.user.$loaded().then(function() {
		if($scope.user.username == null){
			$timeout(function() {
				$scope.existeix=false; 
			})
		}
	})
	var refConfUser= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID + "/acces");
	var refUsersAcces= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userID +"/usuarisAcces/");
	refConfUser.on("value", function(snapshot) {
	  	switch(snapshot.val()) {
		    case "privat":
		   	 	$timeout(function() {
		    		$scope.tipusAcces= "Privat"
		    	})
		        console.log("Es privat");
		        if(!($scope.permis)){
		        	console.log("No tens accés")
		        	$scope.tipusPermis= "privat";
		        }
		        break;
		    case "public":
		    	$timeout(function() {
		    		$scope.tipusAcces= "Públic"
		    	})
		        console.log("Es public");
		        	$scope.permis=true;
		        break;
		    case "alguns":
		    	$timeout(function() {
		    		$scope.tipusAcces= "Restringit per alguns usuaris"
		    	})
		        console.log("Es alguns");
		        if(!($scope.permis)){
		        	var usuarisAcces= $firebaseArray(refUsersAcces); 
		        	usuarisAcces.$loaded().then(function() {
		        		var res= usuarisAcces.$indexFor($scope.userSessio.username);
		        		if(res== -1){
							if($scope.userSessio== "anonim"){
								console.log("No tens accés, però pots fer login")
								$scope.tipusPermis= "algunsPotLog";
							}
		        			else{
		        				console.log("No tens accés")
		        				$scope.tipusPermis= "algunsNoTu";
		        			}
		        		}
		        		else{
		        			$scope.permis=true;
		        		}
		        	})
		        }
		} 
	}, function (errorObject) {
	  console.log("The read failed: " + errorObject.code);
	});
	$scope.traduirData= function(d){
    	return new Date(d)
  	}
  	$scope.noAcces= function(){
  		if($scope.tipusPermis=="privat") {
  			$location.path("/acces/PerfilPrivat");
  		}
  		else if($scope.tipusPermis=="algunsPotLog") {
  			$location.path("/acces/PerfilLogin");
  		}
  		else if($scope.tipusPermis=="algunsNoTu") {
  			$location.path("/acces/PerfilLlista");	
  		}
  	}
  	$scope.open = function () {
      var modalInstance = $modal.open({
        templateUrl: 'ElimCompteContent.html',
        controller: 'ElimCompteInstanceCtrl',
        resolve: {
	        ref: function () {
	          return ref;
	        },
	        email: function () {
	          return authData.password.email;
	        },
	        userIDSessio: function () {
	          return $scope.userIDSessio;
	        }
	    }
    });
    modalInstance.result.then(function () {
    	$location.path("/");
    }, function () {
      console.log("Model dismiss");
    });
    $scope.$on('$routeChangeStart', function(event, newUrl, oldUrl) {
      	if (modalInstance) {
        	modalInstance.dismiss('cancel');
      	}
    });
  };
  $scope.loading=false;
}])

.controller('ElimCompteInstanceCtrl',['$scope','$modalInstance','ref','email', '$timeout','userIDSessio','$firebaseObject', '$firebaseArray', function ($scope, $modalInstance,ref,email, $timeout,userIDSessio,$firebaseObject,$firebaseArray) {
  var email= email;
  var ref= ref;
  $scope.userIDSessio=userIDSessio;
  $scope.passError=false;
  $scope.passErrorMessage="";
  $scope.working= false;
  var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
  $scope.userSessio= $firebaseObject(refUserSessio);
  $scope.userSessio.$loaded().then(function() {
  	$scope.username= $scope.userSessio.username;
  })
  $scope.ok = function () { 
  		$scope.working= true;
  		var pass= $scope.passElim;
  		ref.authWithPassword({
		  email    : email,
		  password : pass
		}, function(error, authData) {
		  	if (error) {
		  		$timeout(function() {
					$scope.passError=true;
		  			$scope.passErrorMessage="La contrasenya no es correcte";			  			
		  			$scope.working= false;
				});
			}
			else{
				var refConfAccesUsuari= new Firebase("https://tfgcodisqr.firebaseio.com/confAccesPerfil/"+ $scope.userIDSessio);
    			var refUsurReg= new Firebase("https://tfgcodisqr.firebaseio.com/uRegs/"+ $scope.username );
    			var refUserSessio= new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio);
    			$scope.conversesUsuari=  $firebaseArray(new Firebase("https://tfgcodisqr.firebaseio.com/users/"+ $scope.userIDSessio + "/converses"));
    			$scope.arrayConverseElim=[]
    			$scope.conversesUsuari.$loaded().then(function() {
    				var numConvElim=0;
    				function elimanarConversa(conv){
			  			var k = $scope.arrayConverseElim.indexOf(conv)
			  			if($scope.arrayConverseElim[k].contadorUsersActius<2){
			  				$scope.arrayConverseElim[k].$remove()
			  			}
			  			else{
			  				$scope.arrayConverseElim[k].contadorUsersActius= $scope.arrayConverseElim[k].contadorUsersActius-1;
							$scope.arrayConverseElim[k].$save()
			  			}
			  			numConvElim+=1;
			  			if(numConvElim==$scope.conversesUsuari.length){
			  				///Quan s'han tractat totes les nostres converses llavors podem eliminar l'usuari
			  				//Si no s'eliminava primer el usuari i amb l'usuari eliminat no podiem tractar les converses
			  				eliminarUsuari()
			  			}
			  		}
			  		function eliminarUsuari(){
			  				refConfAccesUsuari.remove();
			    			refUsurReg.remove();
			    			refUserSessio.remove();
			    			ref.removeUser({
						 		email: email,
						 		password: pass
							}, function(error) {
						  		if (error === null) {
						    		console.log("User removed successfully");
						    		$modalInstance.close();
						  		} else {
						    		console.log("Error removing user:", error);
						  		}
					  		});
			  		}
			  		if($scope.conversesUsuari.length>0){
	    				for (var i = 0; i < $scope.conversesUsuari.length; i++) {
	        				var IDConv= $scope.conversesUsuari[i].$id //ID de la conversa
	        				$scope.convEliminar= $firebaseObject(new Firebase("https://tfgcodisqr.firebaseio.com/converses/"+ IDConv));
	        				$scope.arrayConverseElim.push($scope.convEliminar) 
	        				$scope.arrayConverseElim[i].$loaded().then(function(conv) {
					            elimanarConversa(conv)
					        })  
	    				}
	    			}
	    			else{
	    				eliminarUsuari()
	    			}
    			})
			}
		  })

  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
 }])